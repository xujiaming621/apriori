import java.util.LinkedList;
import java.util.List;

/**
 * @author Xu,
 * @data 2020/11/15，
 * @time 16:08
 * 自定义的map类，一个对象存放一个频繁项集以及其支持度计数
 */
public class  Mymap{
    public List<String> li=new LinkedList<>();
    public int count;

    /**
     * 构造函数  新建一个对象
     * @param l
     * @param c
     */
    public Mymap(List<String> l,int c)
    {
        li=l;
        count=c;
    }

    /**
     * 返回得到当前频繁项集的支持度计数
     * @return
     */
    public int getcount()
    {
        return count;
    }

    /**
     * 判断传入的频繁项集是否和本频繁项集相同
     * @param in
     * @return
     */
    public boolean isListEqual(List<String> in)
    {
        //先判断大小是否相同
        if(in.size()!=li.size()) {
            return false;
        }
        else {
            //遍历输入的频繁项集，判断是否所有元素都包含在本频繁项集中
            for(int i=0;i<in.size();i++)
            {
                if(!li.contains(in.get(i))) {
                    return false;
                }
            }
        }
        //如果两个频繁项集大小相同，同时本频繁项集包含传入的频繁项集的所有元素，则表示两个频繁项集是相等的，返回为真
        return true;
    }
}

